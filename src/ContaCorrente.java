
public class ContaCorrente {

	private float saldo = 0;
	private float valor1;
	
	public float getSaldo(){
		return saldo;
	}
	public synchronized void depositar(float valor){
		valor1 = getSaldo();
		saldo = valor1 + valor;
	}
	public synchronized void sacar(float valor){
		valor1 = getSaldo();
		if(valor1 < valor){
			throw new IllegalArgumentException("Saldo insuficiente!");
		}
		else{
			saldo = valor1 - valor;
			System.out.println("Seu saque foi efetuado!");
		}
	}
	
}
