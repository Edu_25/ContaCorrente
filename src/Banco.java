
public class Banco {

	public static void main(String[] args){
		ContaCorrente conta = new ContaCorrente();
		Fornecedor fornece = new Fornecedor(conta);
		Clientes cliente = new Clientes(conta);
		
		System.out.println("Saldo: " + conta.getSaldo());
		fornece.start();
		cliente.start();
		while(fornece.getState() != Thread.State.TERMINATED || cliente.getState() != Thread.State.TERMINATED){
			
		}
		
		System.out.println("Último Saldo: "+conta.getSaldo());
	}
}
